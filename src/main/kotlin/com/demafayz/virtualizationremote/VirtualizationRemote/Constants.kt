package com.demafayz.virtualizationremote.VirtualizationRemote

class Constants {

    object SOCKET {
        var PORT = 8080;
        var PROTOCOL = "ws"
        var ADDRESS = "localhost"
        var URL = "$PROTOCOL://$ADDRESS:$PORT"
    }
}