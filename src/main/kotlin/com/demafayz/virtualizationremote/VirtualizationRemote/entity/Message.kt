package com.demafayz.virtualizationremote.VirtualizationRemote.entity

data class Message(val from : String, val message : String)