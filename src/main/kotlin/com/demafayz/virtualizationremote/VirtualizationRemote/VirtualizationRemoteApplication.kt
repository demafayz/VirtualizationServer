package com.demafayz.virtualizationremote.VirtualizationRemote

import com.demafayz.virtualizationremote.VirtualizationRemote.net.Server
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class VirtualizationRemoteApplication

fun main(args: Array<String>) {
    println("Hello World!")
    startServer()
//    SpringApplication.run(VirtualizationRemoteApplication::class.java, *args)
}

fun startServer() {
    val server = Server(Constants.SOCKET.PORT)
    server.start()
}
