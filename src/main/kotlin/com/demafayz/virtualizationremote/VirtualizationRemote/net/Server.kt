package com.demafayz.virtualizationremote.VirtualizationRemote.net

import org.java_websocket.WebSocket
import org.java_websocket.handshake.ClientHandshake
import org.java_websocket.server.WebSocketServer
import java.lang.Exception
import java.net.InetSocketAddress

class Server : WebSocketServer {

    constructor(port : Int) : super(InetSocketAddress(port))

    constructor(address : InetSocketAddress) : super(address)

    override fun onOpen(conn: WebSocket?, handshake: ClientHandshake?) {
        broadcast("new connection: ${handshake?.resourceDescriptor}")
        println("${conn?.remoteSocketAddress?.address?.hostAddress}  entered the room!")
    }

    override fun onClose(conn: WebSocket?, code: Int, reason: String?, remote: Boolean) {
        broadcast("$conn has left the room!")
        println("$conn has left the room!")
    }

    override fun onMessage(conn: WebSocket?, message: String?) {
        broadcast(message)
        println("$conn: $message")
    }

    override fun onError(conn: WebSocket?, ex: Exception?) {
        ex?.printStackTrace()
    }

    override fun onStart() {
        println("Server started!")
    }
}