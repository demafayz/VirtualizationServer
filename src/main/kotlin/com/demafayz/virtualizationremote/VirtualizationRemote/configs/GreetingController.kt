package com.demafayz.virtualizationremote.VirtualizationRemote.configs

import com.demafayz.virtualizationremote.VirtualizationRemote.entity.Message
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.stereotype.Controller

@Controller
class GreetingController {

    @MessageMapping("/message")
    @SendTo("/chat/messages")
    fun greeting(message : Message) : Message {
        Thread.sleep(3000)
        println("Socket message: ${message.toString()}")
        return message
    }
}